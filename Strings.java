//1.Find the number of words

public class WordCount {  
      static int wordcount(String string)  
        {  
          int count=0;  
      
            char ch[]= new char[string.length()];     
            for(int i=0;i<string.length();i++)  
            {  
                ch[i]= string.charAt(i);  
                if( ((i>0)&&(ch[i]!=' ')&&(ch[i-1]==' ')) || ((ch[0]!=' ')&&(i==0)) )  
                    count++;  
            }  
            return count;  
        }  
      public static void main(String[] args) {  
          String string ="    India Is My Country";  
         System.out.println(wordcount(string) + " words.");   
    }  
}  

//2.print the number of characters in a string

public class CountCharacter    
{    
    public static void main(String[] args) {    
        String string = "The best of both worlds";    
        int count = 0;       
        for(int i = 0; i < string.length(); i++) {    
            if(string.charAt(i) != ' ')    
                count++;    
        }    
    System.out.println("Total number of characters in a string: " + count);    
    }    
}     

//3.print the number of special characters, lowercase and uppercase

public class Sample2 {
   public static void main(String args[]) {
      String data = "Hello HOW are you MR 51";
      char [] charArray = data.toCharArray();
      int upper = 0;
      int lower = 0;
      int digit = 0;
      int others = 0;

      int totalChars = data.length();
      for(int i=0; i<data.length(); i++) {
         if (Character.isUpperCase(charArray[i])) {
            upper++;
         } else if(Character.isLowerCase(charArray[i])) {
            lower++;
         } else if(Character.isDigit(charArray[i])){
            digit++;
         } else {
            others++;
         }
      }
      System.out.println("Total length of the string :"+totalChars);
      System.out.println("Upper case :"+upper);
      System.out.println("Percentage of upper case letters: "+(upper*100)/totalChars);
      System.out.println("Lower case :"+lower);
      System.out.println("Percentage of lower case letters:"+(lower*100)/totalChars);
      System.out.println("Digit :"+digit);
      System.out.println("Percentage of digits :"+(digit*100)/totalChars);
      System.out.println("Others :"+others);
      System.out.println("Percentage of other characters :"+(others*100)/totalChars);
   }
}

//4.find the number of letters in the string

public class CountCharacter    
{    
    public static void main(String[] args) {    
        String string = "The best of both worlds";    
        int count = 0;      
        for(int i = 0; i < string.length(); i++) {    
            if(string.charAt(i) != ' ')    
                count++;    
        }    
          System.out.println("Total number of characters in a string: " + count);    
    }    
}    

//6.Convert all words in title case

public class StringExample
{
    public static void main(String[] args)
    {
 
        final char[] delimiters = { ' ', '_' };
 
        WordUtils.capitalizeFully(null, delimiters);
        WordUtils.capitalizeFully(" ", delimiters);
        WordUtils.capitalizeFully("a", delimiters);
        WordUtils.capitalizeFully("thor almighty", delimiters);
        WordUtils.capitalizeFully("string operation", delimiters);
        WordUtils.capitalizeFully("TITLE CASE CONVERSION", delimiters);
    }
}

//7.Take a Email address from user and check if it is valid

public class Demo {
   static boolean isValid(String email) {
      String regex = "                ";
      return email.matches(regex);
   }
   public static void main(String[] args) {
      String email = "john123@gmail.com";
      System.out.println("The E-mail ID is: " + email);
      System.out.println("Is the above E-mail ID valid? " + isValid(email));
   }
}


//8.Take a password from user

import java.io.Console;
public class Example42 {
	public static void main(String[] args) {
		Console cons;
		if ((cons = System.console()) != null) {
			char[] pass_ward = null;
			try {
				pass_ward = cons.readPassword("Input your Password:");
				System.out.println("Your password was: " + new String(pass_ward));
			} finally {
				if (pass_ward != null) {
					java.util.Arrays.fill(pass_ward, ' ');
				}
			}
		} else {
			throw new RuntimeException("Can't get password...No console");
		}
	}
}

//13.Find duplicate characters in a String

public class DuplStr {
 public static void main(String argu[]) {

  String str = "Venkatesh1234";
  int cnt = 0;
  char[] inp = str.toCharArray();
  System.out.println("Duplicate Characters are:");
  for (int i = 0; i < str.length(); i++) {
   for (int j = i + 1; j < str.length(); j++) {
    if (inp[i] == inp[j]) {
     System.out.println(inp[j]);
     cnt++;
     break;
    }
   }
  }
 }
}

