//1. Print1_N
import java.util.Scanner;
public class Print1_N {
	  public static void main(String[] args)
		{
			int i =1;
			Scanner Sc = new Scanner(System.in);
			System.out.print("Enter the value n : ");
			int n = Sc.nextInt();

			System.out.println("Numbers are : ");

			while(i<=n)
			{
				System.out.println(i);
				i++;
			}
		}
	}

//2.PrintN_1

import java.util.Scanner;

public class PrintN_1 {
	private static Scanner sc;
	public static void main(String[] args) 
	{
		int number, i;
		Scanner sc = new Scanner(System.in);
		
		System.out.print(" Please Enter the Maximum integer Value : ");
		number = sc.nextInt();	
		
		i = number;
		
		while(i >= 1)
		{
			System.out.print(i +" "); 
			i--;
		}	
	}
}

//3.Character

public class Character {

    public static void main(String[] args) {

        char c;

        for(c = 'A'; c <= 'Z'; ++c)
            System.out.print(c + " ");
    }
}

//4.Evenumbers1_100

import java.util.Scanner;

public class Evenumbers1_100{
	private static Scanner sc;
	public static void main(String[] args) 
	{
		int number, i;
		Scanner sc = new Scanner(System.in);
		
		System.out.print(" Please Enter any Number : ");
		number = sc.nextInt();	
		
		for(i = 1; i <= number; i++)
		{
			if(i % 2 == 0)
			{
				System.out.print(i +"\t"); 
			}
		}	
	}
}

//5.Odd1_100

public class Odd1_100 {
		public static void main(String args[]) {
			System.out.println("The Odd Numbers are:");
			for (int i = 1; i <= 100; i++) {
				if (i % 2 != 0) {
					System.out.print(i + " ");
				}
			}
		}
	}

//6.Natural 

import java.util.Scanner;

public class Natural 
{
    public static void main(String args[])
    {
        int x, i = 1 ;
        int sum = 0;
        System.out.println("Enter Number of items :");
        Scanner s = new Scanner(System.in);
        x = s.nextInt();
        while(i <= x)
        {
            sum = sum +i;
            i++;
        }
        System.out.println("Sum of "+x+" numbers is :"+sum);
    } 
}
//7.SumOfEveNumbers

public class SumOfEveNumbers {
		public static void main(String[] args){
		int sum=0;
		for(int i=10; i<=100; i=i+2){
			sum = sum + i;
		}
		System.out.println(sum);
		}
	}
//8.OddNumbers1_N

public class OddNumbers1_N {
		public static void main(String args[]) 
		{
			int sum = 0;
			for (int i = 1; i <= 100; i++) 
			{
				if (i % 2 != 0) 
				{
					sum = sum + i;
				}
			}
			System.out.println("The Sum Of 100 Odd Numbers are:" + sum);
		}
	}
//9. Multiplication_Table 
	

import java.util.Scanner;
	public class Multiplication_Table 
	{
	    public static void main(String[] args) 
	    {
	        Scanner s = new Scanner(System.in);
		System.out.print("Enter number:");        
		int n=s.nextInt();
	        for(int i=1; i <= 10; i++)
	        {
	            System.out.println(n+" * "+i+" = "+n*i);
	        }
	    }
	}

//10. NumberDigits

public class NumberDigits {

    public static void main(String[] args) {

        int count = 0, num = 3452;

        while(num != 0)
        {
            num /= 10;
            ++count;
        }

        System.out.println("Number of digits: " + count);
    }
}